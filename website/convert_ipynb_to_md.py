"""
Poor man's script to build the markdown version of the examples.

Requires Python 3.6.
"""
import fnmatch
import os
from pathlib import Path
import re
import shutil

OUTPUT_NB_PATH = Path("./docs")
if not OUTPUT_NB_PATH.exists():
    os.makedirs(OUTPUT_NB_PATH)

# IGNORE_FILES = ["Tutorial_Index.md", "installation.md", "salvus_compute_api.md", "units.md", "visualizing.md"]
# IGNORE_FILES = [OUTPUT_NB_PATH / _i for _i in IGNORE_FILES]

# File-types to also copy over.
FILETYPES_TO_COPY = ["*.png", "*.jpg", "*.jpeg"]

# Possibility to exclude some notebooks based on pattern matching.
EXCLUDE_NBS_PATTERNS = []


def patch_markdown_file(md):
    """
    Change the markdown files a bit to look nicer.
    """

    with open(md, "rt") as fh:
        t = fh.read()

    # Remove the html header.
    t = re.sub(r"<div[\s\S]*<\/div>", "", t, flags=re.MULTILINE)

#    t = ("!!! tip \"Download Jupyter Notebook\"\n\n"
#         "\tThis page is automatically generated from a Jupyter "
#         f"Notebook.\n\n\tYou can find all notebooks in the subfolder \"examples\""
#         "in this repository.") + t

    with open(md, "wt") as fh:
        fh.write(t)


try:
    # Find all notebooks.
    ipynb_root = Path("../examples")
    _notebooks = list(ipynb_root.glob("*.ipynb"))

    root_folder = Path(os.path.commonprefix(_notebooks))
    if not Path.exists(root_folder):
        root_folder = root_folder.parent

    all_nbs = []
    for nb in _notebooks:
        for pattern in EXCLUDE_NBS_PATTERNS:
            if fnmatch.fnmatch(nb, pattern):
                break
        else:
            all_nbs.append(nb)

    for nb in all_nbs:
        print(nb.stem)
        # Keep nesting of notebooks.
        out = OUTPUT_NB_PATH / nb.stem / nb.relative_to(root_folder).parent
        print(out)
        os.system(f"jupyter-nbconvert --output-dir={out} --to markdown '{nb}'")

        # Also copy over any files required by the notebooks (mostly images).
        for t in FILETYPES_TO_COPY:
            files = list(nb.parent.glob("**/" + t))
            for f in files:
                f_out = out / f.relative_to(nb.parent)
                if not f_out.parent.exists():
                    os.makedirs(f_out.parent)
                shutil.copy(f, f_out)

    # Modify the markdown files a bit.
    for md in OUTPUT_NB_PATH.glob("**/*.md"):
        patch_markdown_file(md)
finally:
    print('done')
