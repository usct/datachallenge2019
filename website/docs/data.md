# Synthetic Waveform Data


### 2D ring aperture with known ground truth

This dataset is meant to facilitate testing and calibration of the reconstruction algorithms.

You can download waveform data for measurements in water and with a phantom.

The numerical phantom used in the simulations can be reprodcued by follwing this [example](/Example-Simple-SyntheticPhantom/Example-Simple-SyntheticPhantom/) and using the tools from the [git repository](https://gitlab.com/usct/datachallenge2019).

[:file_folder: phantom (1.8 GB)](https://s3.eu-central-1.amazonaws.com/usctdatachallenge2019/2D-simple-phantom/phantom_simple.h5)

[:file_folder: water (1.8 GB)](https://s3.eu-central-1.amazonaws.com/usctdatachallenge2019/2D-simple-phantom/water.h5)

 ---

### 2D ring aperture blind test

This dataset comprises waveform data for measurements in water and with a synthetic phantom provided by Torsten Hopp (KIT).

All waveforms are raw simulation output, i.e., without applying any sort of filtering or processing.
The computations are accurate up to 5 MHz.

[:file_folder: phantom (1.8 GB)](https://s3.eu-central-1.amazonaws.com/usctdatachallenge2019/2D-blind-test-A/phantom.h5)

[:file_folder: water (1.8 GB)](https://s3.eu-central-1.amazonaws.com/usctdatachallenge2019/2D-blind-test-A/water.h5)


---

### 3D aperture blind test

Coming soon.
