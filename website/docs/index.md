# Welcome to the USCT Data Challenge 2019

To  foster  the  exchange  of  knowledge and reproducible science, the [USCT data exchange and collaboration team](http://ipeusctdb1.ipe.kit.edu/~usct/challenge/) organizes a new data challenge using synthetic waveform data from numerical phantoms.
Using this database, we invite all participants to benchmark different imaging algorithms in terms of the quality of the reconstruction and computational efficiency. These pages collect information on software tools and data access for the blind test.

---

## Software and tools

We started a collection of software tools to create, access and process USCT data in the following [git repository](https://gitlab.com/usct/datachallenge2019).

---

## Waveform data

The first data set has been released. Check out the [data](data) section and stay tuned for updates.


---

## Previous data challenge

[Here](http://ipeusctdb1.ipe.kit.edu/~usct/challenge/?page_id=262) is an overview of the USCT Data Challenge 2017.
