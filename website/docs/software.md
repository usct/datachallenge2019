This repository contains a collection of scripts to create input data for a synthetic USCT database.
This includes transducer specifications, numerical breast phantoms and an initial attempt to come up with
a general file format to store USCT waveform data.

---

Contents of the subfolders:

## Apertures

Tools to create different acquisition geometries with predefined emitter and receiver locations.


## Phantom generator

Tools to create numerical breast phantoms with simple geometries and inclusions. Additionally, external phantoms can be read and processed for 2D and 3D domains. We provide utilities to project the phantoms onto arbitrary structured and unstructured meshes.
The space-dependent tissue properties can be retrieved either as a call-back function for
maximum flexibility, or on a rectilinear mesh, which is then written to an `hdf5` file that can be visualized in Paraview.


## Data format

The suggested data format is based on `hdf5` and inspired by the [Adaptable Seismic Data Format (ASDF)](https://seismic-data.org/).
The main focus is portability across platforms and acquisition systems with the goal of creating comprehensive data sets including all necessary meta information. Interfaces for Python, Matlab, C++ are available. A Fortran interface will follow soon.


---


## Installation instructions

Most of the tools are based on python. To install all required dependencies, we recommend to create a new
python environment using miniconda.


Download the miniconda installer for your operating system from here [https://conda.io/miniconda.html](https://conda.io/miniconda.html)


**Windows**

Follow the installation instructions and open the Anaconda prompt when the installation is finished.

**Linux and OSX**

```bash
# Make the installation script executable.
chmod +x Miniconda3*.sh
# Run the script and follow the instructions.
./Miniconda3*.sh
```

---

On all operating systems continue now in the terminal / Anaconda prompt with


```bash
#####
# Create conda environment for the USCT Database
#####
conda config --prepend channels conda-forge
conda create -n usct python=3.6
source activate usct

#####
# Install dependencies
#####
conda install toml obspy h5py pandas pyyaml pathlib
```

It is convenient to look at the examples with interactive Jupyter widgets. To use them you have to install a few more packages.

```bash
#####
# Additional packages to use the Jupyter widgets
#####
# Install with conda as it takes care to properly register the plugins.
conda install jupyter jupyter_contrib_nbextensions ipywidgets pythreejs pandas qgrid bqplot
# This results in easier to read error messages in the Jupyter notebooks.
jupyter nbextension enable skip-traceback/main

```

## Visualization

[Paraview](https://www.paraview.org/) is a powerful tool for visualizing scientific data in 2D and 3D.
It is freely available for download from here: [https://www.paraview.org/download/](https://www.paraview.org/download/).

Select the latest pre-built version for your operating system (v5.6). No installation is required.



